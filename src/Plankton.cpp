#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <OSCBundle.h>
#include <OSCBoards.h>
#include <OSCMessage.h>
#include <OneButton.h>

#include "../src/Potentiometer.h"
#include "../src/NetworkWatchDog.h"

AudioInputI2S in;
AudioEffectFreeverb freeverbR;
AudioEffectFreeverb freeverbL;
AudioMixer4 mixerL;
AudioMixer4 mixerR;
AudioOutputI2S out;
AudioConnection patchCord1(in, 0, freeverbL, 0);
AudioConnection patchCord2(in, 0, mixerL, 1);
AudioConnection patchCord3(in, 1, freeverbR, 0);
AudioConnection patchCord4(in, 1, mixerR, 1);
AudioConnection patchCord5(freeverbR, 0, mixerR, 0);
AudioConnection patchCord6(freeverbL, 0, mixerL, 0);
AudioConnection patchCord7(mixerL, 0, out, 0);
AudioConnection patchCord8(mixerR, 0, out, 1);
AudioControlSGTL5000 sgtl5000_1;

//Circuit:
EthernetUDP Udp;
NetworkWatchDog networkWatchDog(Udp, 5000);
//port numbers
const uint16_t inPort = 8888;
//Karen
int karensPort = 0;

//Potentiometers
Potentiometer decayPotentiometer(A1);
Potentiometer dampingPotentiometer(A2);
Potentiometer mixPotentiometer(A3);

void sendOSCMessage(OSCMessage &message, uint16_t outPort)
{
	if(networkWatchDog.state != EthernetModuleState::BAD_HARDWARE) 
	{
		Udp.beginPacket(Udp.remoteIP(), outPort);
		message.send(Udp); // send the bytes to the SLIP stream
		Udp.endPacket();   // mark the end of the OSC Packet
	}
}

void setMix(float value)
{
	mixerL.gain(0, value);
	mixerR.gain(0, value);

	mixerL.gain(1, 1 - value);
	mixerR.gain(1, 1 - value);

	Serial.println(value);
}

void lets_play(OSCMessage &message, int addrOffset)
{
	if (message.isInt(0))
	{
		int port = message.getInt(0);
		if (karensPort == 0 || karensPort == port)
		{
			karensPort = port;

			OSCMessage msg("/plankton/lets_play");
			msg.add("ok").add(teensySN());
			sendOSCMessage(msg, port);

			Serial.print("Remote IP address: ");
			Serial.println(Ethernet.localIP());
			Serial.print("Remote Port: ");
			Serial.print(karensPort);
		}

		Serial.print("Remote IP address: ");
		Serial.println(Udp.remoteIP());
	}
}

void where_are_you(OSCMessage &message, int addrOffset)
{
	Serial.print("/where_are_you");
	if (message.isInt(0))
	{
		OSCMessage msg("/plankton/where_are_you");
		msg.add("delay_reverb")
			.add((Ethernet.localIP()[0] + String(".") + Ethernet.localIP()[1] + String(".") + Ethernet.localIP()[2] + String(".") + Ethernet.localIP()[3]).c_str())
			.add(teensySN());
		sendOSCMessage(msg, message.getInt(0));
	}
}

void bye(OSCMessage &message, int addrOffset)
{
	if (message.getInt(0) && message.getInt(0) == karensPort)
	{
		Serial.println("bye");
		karensPort = 0;
	}
}

void decay(OSCMessage &message, int addrOffset)
{
	//Serial.print("/decay");
	//Serial.printf("%.6f \n", message.getFloat(0));
}

void damping(OSCMessage &message, int addrOffset)
{
	float valueToSet = message.getFloat(0);
	//Serial.print("/damping");
	//Serial.printf("%.6f \n", message.getFloat(0));
	freeverbR.roomsize(valueToSet);
	freeverbL.roomsize(valueToSet);
}

void mix(OSCMessage &message, int addrOffset)
{
	float valueToSet = message.getFloat(0);
	//Serial.print("/mix");
	//Serial.printf("%.6f \n", valueToSet);
	setMix(valueToSet);
}

void setup()
{
	// allocate enough memory for the delay
	AudioMemory(10);
	if(networkWatchDog.state != EthernetModuleState::BAD_HARDWARE) 
	{

	Udp.begin(inPort);
	}

	// enable the audio shield
	sgtl5000_1.enable();
	sgtl5000_1.volume(0.9);
	sgtl5000_1.adcHighPassFilterDisable();
	sgtl5000_1.muteHeadphone();

	decayPotentiometer.attachOnChange([](int value) {
		float valueToSet = (float)value / 255;
		freeverbR.roomsize(valueToSet);
		freeverbL.roomsize(valueToSet);
		//Serial.println(valueToSet);
		if (karensPort == 0)
			return;
		OSCMessage msg("/decay");
		msg.add(valueToSet);
		sendOSCMessage(msg, karensPort);
	});

	dampingPotentiometer.attachOnChange([](int value) {
		float valueToSet = (float)value / 255;
		freeverbR.roomsize(valueToSet);
		freeverbL.roomsize(valueToSet);
		//Serial.println(valueToSet);
		if (karensPort == 0)
			return;
		OSCMessage msg("/damping");
		msg.add(valueToSet);
		sendOSCMessage(msg, karensPort);
	});

	mixPotentiometer.attachOnChange([](int value) {
		float valueToSet = (float)value / 255;
		// set mix
		setMix(valueToSet);

		if (karensPort == 0)
			return;
		OSCMessage msg("/mix");
		msg.add(valueToSet);
		sendOSCMessage(msg, karensPort);
	});
}

void loop() {
	if(networkWatchDog.state != EthernetModuleState::BAD_HARDWARE) {
		OSCBundle bundleIN;
		int size;
		if ((size = Udp.parsePacket()) > 0) {
			while (size--) {
				bundleIN.fill(Udp.read());
			}
			Serial.print("/ok?");
			if (!bundleIN.hasError()) {
				Serial.print("/ok");
				bundleIN.route("/karen/where_are_you", where_are_you);
				bundleIN.route("/karen/lets_play", lets_play);
				bundleIN.route("/karen/bye", bye);
				bundleIN.route("/decay", decay);
				bundleIN.route("/damping", damping);
				bundleIN.route("/mix", mix);
			} else {
				Serial.print("/error");
				switch (bundleIN.getError())
				{
				case 1:
					Serial.println("BUFFER_FULL");
					break;
				case 2:
					Serial.println("INVALID_OSC");
					break;
				case 3:
					Serial.println("ALLOCFAILED");
					break;
				case 4:
					Serial.println("INDEX_OUT_OF_BOUNDS");
					break;
				}
			}
		}
	}
	decayPotentiometer.tick();
	dampingPotentiometer.tick();
	mixPotentiometer.tick();
	networkWatchDog.watch();
}
