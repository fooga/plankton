#pragma once

#include "AudioStream.h"

class Reverb : public AudioStream
{
public:
        Reverb() : AudioStream(1, inputQueueArray){}
        virtual void update(void);
private:
        audio_block_t *inputQueueArray[1];
};