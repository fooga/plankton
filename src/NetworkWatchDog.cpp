#include "NetworkWatchDog.h"

NetworkWatchDog::NetworkWatchDog(EthernetUDP &ethernetUDP, unsigned long timeout)
	: udp(ethernetUDP), lastWatchedTime(0), timeout(timeout)
{
	teensyMAC(mac);
	Ethernet.init(10);
	// use alternate pins for these  SPI signals
	SPI.setMOSI(7);
	SPI.setSCK(14);
	state = EthernetModuleState::OK;
	initializeEthernetWithDHCP();
}

NetworkWatchDog::~NetworkWatchDog()
{
}

void NetworkWatchDog::initializeEthernetWithDHCP() {
	if (Ethernet.begin(mac, 100, 10) == 0) {
		// Failed to configure Ethernet using DHCP
		if (Ethernet.hardwareStatus() == EthernetNoHardware) {
			// Ethernet shield was not found.  Sorry, can't run without hardware. :(
			state = EthernetModuleState::BAD_HARDWARE;
			return;
		} else if (Ethernet.linkStatus() == LinkOFF) {
			// Ethernet cable is not connected.
			state = EthernetModuleState::BAD_CABEL;
			IPAddress ip(192, 168, 1, 177);
			IPAddress myDns(192, 168, 1, 1);
			IPAddress gateway(192, 168, 1, 1);
			IPAddress subnet(255, 255, 0, 0);
			// initialize the Ethernet device not using DHCP:
			Ethernet.begin(mac, ip, myDns, gateway, subnet);
		}
	}
}

void NetworkWatchDog::watch()
{
	if (state == EthernetModuleState::BAD_HARDWARE)
	{
		return;
	}
	if ((millis() - lastWatchedTime) > timeout)
	{
		sayWoof();
		lastWatchedTime = millis();
	}
}

void NetworkWatchDog::sayWoof()
{
	Serial.println("Woof!");
	switch (Ethernet.maintain())
	{
	case 1:
		//renewed fail
		Serial.println("Error: renewed fail");
		break;

	case 2:
		//renewed success
		Serial.println("Renewed success");
		//print your local IP address:
		Serial.print("My IP address: ");
		Serial.println(Ethernet.localIP());
		break;

	case 3:
		//rebind fail
		Serial.println("Error: rebind fail");
		break;

	case 4:
		//rebind success
		Serial.println("Rebind success");
		//print your local IP address:
		Serial.print("My IP address: ");
		Serial.println(Ethernet.localIP());
		uint16_t port = udp.localPort();
		udp.stop();
		udp.begin(port);
		break;
	}
}
