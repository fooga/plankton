#include "Potentiometer.h"

Potentiometer::Potentiometer(byte pin)
{
	_pin = pin;
	_value = analogRead(_pin);
	_value = _value >> 2;
	_oldValue = _value << 2;
	_value = _value << 2;
}

Potentiometer::~Potentiometer() {}

byte Potentiometer::getValue()
{
	_value = analogRead(_pin);
	int tmp = (_oldValue - _value);
	if (tmp >= 6 || tmp <= -6)
	{
		_oldValue = _value >> 2;
		_oldValue = _oldValue << 2;
		return _value >> 2;
	}
	return 255;
}

void Potentiometer::tick()
{
	if (_callbackFunction)
	{
		byte value = getValue();
		if (value != 255)
		{
			_callbackFunction(value);
		}
	}
}

// save function for change event
void Potentiometer::attachOnChange(void (*callbackFunction)(int value))
{
	_callbackFunction = callbackFunction;
}