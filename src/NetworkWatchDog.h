#pragma once
#include <TeensyID.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <SPI.h>

enum EthernetModuleState
{
    BAD_HARDWARE,
    BAD_CABEL,
    OK
};

class NetworkWatchDog
{
public:
	NetworkWatchDog(EthernetUDP &ethernetUDP, uint32_t timeout);
	~NetworkWatchDog();
	uint8_t mac[6];
	EthernetModuleState state;
	void watch();

private:

	void sayWoof();
	void initializeEthernetWithDHCP();
	EthernetUDP &udp;
	uint32_t lastWatchedTime;
	uint32_t timeout;



};

