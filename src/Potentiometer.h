#pragma once
#include <Arduino.h>

class Potentiometer
{
public:
	Potentiometer(byte pin);
	~Potentiometer();
	byte getValue();
	void tick();
	void attachOnChange(void (*callbackFunction)(int));

private:
	void (*_callbackFunction)(int) = nullptr;
	byte _pin;
	int _value;
	int _oldValue;
};
