#include "Reverb.h" 

void Reverb::update(void)
{
        audio_block_t *block;

		// unity gain, pass input to output without any change
		block = receiveReadOnly(0);

		transmit(block);
		release(block);

}